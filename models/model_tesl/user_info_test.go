/***************************************************
 ** @Desc : This file for ...
 ** @Time : 2019.04.01 12:49 
 ** @Author : Joker
 ** @File : model_test
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.04.01 12:49
 ** @Software: GoLand
****************************************************/
package model_tesl

import (
	"fmt"
	"recharge/models"
	_ "recharge/sys"
	"recharge/utils"
	"testing"
	"time"
)

var globalTest = utils.GlobalMethod{}

var users = models.UserInfo{}

func TestAddUser(t *testing.T) {
	pwd := "are you crazy?."
	users.UserName = "Joker"
	users.LoginName = "Joker"
	users.LoginPwd, users.Salt = globalTest.PasswordSolt(pwd)
	users.CreateTime = globalTest.GetNowTime()
	users.EditTime = users.CreateTime
	users.Authority = 0
	code, _ := users.InsertUsers(users)
	if code > 0 {
		fmt.Println("success!")
	} else {
		fmt.Println("fail!")
	}
}

func TestSelectOneUserDeductionForFail(t *testing.T) {
	userId := -1
	amout := 1
	record := models.WithdrawRecord{}
	record.UserId = -1
	record.MerchantNo = "M200007573"
	record.CreateTime = "2019-04-17 11:49:35"
	record.EditTime = "2019-04-17 11:49:38"
	record.Version = 4
	record.SerialNumber = "20190417114935Eo23OAShrDul"
	record.WhOrderId = "20190417114935Eo23OAShrD"
	record.WhAmount = 1
	record.WhUserType = "1"
	record.WhAccountNo = "6230580000176021868"
	record.WhAccountName = "6230580000176021868"
	record.WhAccountType = "6230580000176021868"
	record.WhBankNo = "6230580000176021868"
	record.Status = utils.S
	record.RecordType = "XF"
	record.Remark = "XF"
	for i := 0; i < 3; i++ {
		go func() {
			fail := users.SelectOneUserDeductionForFail(userId, float64(amout), record)
			fmt.Println("fail:", fail)
		}()
	}
	time.Sleep(1000000)
}
