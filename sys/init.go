/***************************************************
 ** @Desc : This file for ...
 ** @Time : 2019.04.01 12:56 
 ** @Author : Joker
 ** @File : init
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.04.01 12:56
 ** @Software: GoLand
****************************************************/
package sys

import (
	"github.com/astaxie/beego"
)

func InitSystem() {
	//启用Session
	beego.BConfig.WebConfig.Session.SessionOn = true
	//文件存储引擎保存session
	beego.BConfig.WebConfig.Session.SessionProvider = "file"
	beego.BConfig.WebConfig.Session.SessionProviderConfig = "./temp"
	beego.BConfig.WebConfig.Session.SessionName = "JOKERSession"
	beego.BConfig.WebConfig.Session.SessionCookieLifeTime = 3600 //有效时间,秒
	beego.BConfig.WebConfig.Session.SessionGCMaxLifetime = 3600

	//初始化日志
	InitLogs()

	//初始化数据库
	initDatabase := InitDatabase()
	if initDatabase {
		LogNotice("=========== 初始化数据库成功! ============")
	} else {
		LogNotice("=========== 初始化数据库失败! 数据库连接信息错误! ============")
	}
}
