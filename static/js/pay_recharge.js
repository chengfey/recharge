/***************************************************
 ** @Desc : This file for 充值代付js
 ** @Time : 2019.04.10 10:35
 ** @Author : Joker
 ** @File : recharge
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.04.10 10:35
 ** @Software: GoLand
 ****************************************************/

let pay_recharge = {
    query_merchant_type: function (t) {
        $.ajax({
            type: "GET",
            url: "/merchant/merchant_type/" + t,
            cache: false,
            success: function (res) {
                if (res.code === 9) {
                    let con = "";
                    $.each(res.data, function (index, item) {
                        con += "<option value='" + item.Id + "'>" + item.MerchantName + "</option>";

                    });
                    $("#merchant").html(con);
                }
            },
            error: function (XMLHttpRequest) {
                toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
            }
        });
    },
    do_recharge: function () {
        let accountNo = $("#accountNo").val();
        let accountName = $("#accountName").val();
        let money = $("#money").val();
        let patrn2 = /^(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))$/;
        if (accountNo === "" || accountName === "") {
            toastr.error("银行卡或户名不能为空!");
        } else if (!patrn2.exec(money)) {
            toastr.error("请输入正确的充值金额哦!");
        } else if (money !== "") {
            let number = Number(money);
            if (number < 20) {
                toastr.error("充值金额单笔不能小于20哦!");
            } else {
                $.ajax({
                    type: "POST",
                    url: "/merchant/do_recharge_v2/",
                    data: $("#recharge_from").serialize(),
                    cache: false,
                    success: function (res) {
                        if (res.code === 9) {
                            swal("提交成功！", {
                                icon: "success",
                                closeOnClickOutside: false,
                            });
                        } else {
                            swal(res.msg, {
                                icon: "warning",
                                closeOnClickOutside: false,
                            });
                        }
                    },
                    error: function (XMLHttpRequest) {
                        toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
                    }
                });
            }
        }
    },
    do_pay: function () {
        $("#btn_submit_pay").hide();

        let accountNo = $("#accountNo").val();
        let accountName = $("#accountName").val();
        let money = $("#money").val();
        let mobileNo = $("#mobileNo").val();

        let patrn2 = /^(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))$/;

        if (accountNo === "" || accountName === "") {
            toastr.error("银行卡或户名不能为空!");
            $("#btn_submit_pay").show();
        } else if (mobileNo === "") {
            toastr.error("请输入手机验证码!");
            $("#btn_submit_pay").show();
        } else if (money === "") {
            toastr.error("提现金额不能为空哦!");
            $("#btn_submit_pay").show();
        } else if (!patrn2.exec(money)) {
            toastr.error("请输入正确的提现金额哦!");
            $("#btn_submit_pay").show();
        } else {
            let number = Number(money);
            if (number > 45000) {
                toastr.error("代发金额超限!");
            } else {
                $.ajax({
                    type: "POST",
                    url: "/merchant/do_pay_v2/",
                    data: $("#pay_form").serialize(),
                    cache: false,
                    success: function (res) {
                        if (res.code === 9) {
                            swal("提交成功！", {
                                icon: "success",
                                closeOnClickOutside: false,
                            }).then(() => {
                                $("#btn_submit_pay").show();
                            });
                        } else {
                            swal(res.msg, {
                                icon: "warning",
                                closeOnClickOutside: false,
                            }).then(() => {
                                $("#btn_submit_pay").show();
                            });
                        }
                    },
                    error: function (XMLHttpRequest) {
                        toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
                        $("#btn_submit_pay").show();
                    }
                });
            }
        }
    },
    recharge_query: function (no) {
        $.ajax({
            type: "GET",
            url: "/merchant/recharge_query_v2/" + no,
            cache: false,
            success: function (res) {
                if (res.code === 9) {
                    swal(res.msg, {
                        icon: "success",
                        closeOnClickOutside: false,
                    }).then(() => {
                        setInterval(function () {
                            window.location.reload();
                        }, 2000);
                    });
                } else {
                    swal(res.msg, {
                        icon: "warning",
                        closeOnClickOutside: false,
                    });
                }
            },
            error: function (XMLHttpRequest) {
                toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
            }
        });
    },
    recharge_notice: function (no) {
        $.ajax({
            type: "GET",
            url: "/merchant/recharge_notice/" + no,
            cache: false,
            success: function (res) {
                if (res.code === 9) {
                    swal(res.msg, {
                        icon: "success",
                        closeOnClickOutside: false,
                    }).then(() => {
                        setInterval(function () {
                            window.location.reload();
                        }, 2000);
                    });
                } else {
                    swal(res.msg, {
                        icon: "warning",
                        closeOnClickOutside: false,
                    });
                }
            },
            error: function (XMLHttpRequest) {
                toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
            }
        });
    },
    recharge_do_paging: function () {
        let merchantNo = $("#merchant_No").val();
        let accountName = $("#accountName").val();
        let datetimepicker1 = $("#datetimepicker1").val();
        let datetimepicker2 = $("#datetimepicker2").val();
        let uStatus = $("#uStatus").val();
        let _userName = $("#_r_userName").val();
        $.ajax({ //去后台查询第一页数据
            type: "GET",
            url: "/merchant/list_recharge_record/",
            data: {
                page: '1',
                limit: "20",
                MerchantNo: merchantNo,
                _userName: _userName,
                MerchantName: accountName,
                start: datetimepicker1,
                end: datetimepicker2,
                uStatus: uStatus,
            }, //参数：当前页为1
            success: function (data) {
                pay_recharge.show_recharge_data(data.root, data._r_rechargeT);//处理数据

                let options = {//根据后台返回的分页相关信息，设置插件参数
                    bootstrapMajorVersion: 3, //
                    currentPage: data.page, //当前页数
                    totalPages: data.totalPage, //总页数
                    numberOfPages: data.limit,//每页记录数
                    itemTexts: function (type, page) {//设置分页按钮显示字体样式
                        switch (type) {
                            case "first":
                                return "首页";
                            case "prev":
                                return "上一页";
                            case "next":
                                return "下一页";
                            case "last":
                                return "末页";
                            case "page":
                                return page;
                        }
                    },
                    onPageClicked: function (event, originalEvent, type, page) {//分页按钮点击事件
                        $.ajax({//根据page去后台加载数据
                            url: "/merchant/list_recharge_record/",
                            type: "GET",
                            data: {
                                page: page,
                                MerchantNo: merchantNo,
                                _userName: _userName,
                                MerchantName: accountName,
                                start: datetimepicker1,
                                end: datetimepicker2,
                                uStatus: uStatus,
                            },
                            success: function (data) {
                                pay_recharge.show_recharge_data(data.root, data._r_rechargeT);//处理数据
                            }
                        });
                    }
                };
                $('#xf_page').bootstrapPaginator(options);//设置分页
            },
            error: function (XMLHttpRequest) {
                toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
            }
        });
    },
    show_recharge_data: function (list, amount) {
        let con = "";
        $.each(list, function (index, item) {
            let st = "";
            switch (item.Status) {
                case "F":
                    st = "失败";
                    break;
                case "I":
                    st = "处理中";
                    break;
                case "S":
                    st = "成功";
                    break
            }
            let b = "";
            switch (item.ReRecevieBank) {
                case "UPOPJS":
                    b = "银联";
                    break;
                case "NUCC":
                    b = "网联";
                    break;
            }

            con += "<tr><td><a>" + (index + 1) + "</a></td>";
            con += "<td>" + item.UserName + "</td>";
            con += "<td>" + item.ReOrderId + "</td>";
            con += "<td>" + item.ReAmount + "</td>";
            con += "<td>" + item.ReAccountNo + "</td>";
            con += "<td>" + item.ReAccountName + "</td>";
            con += "<td>" + item.EditTime + "</td>";
            con += "<td>" + b + "</td>";
            con += "<td>" + st + "</td>";

            if (item.Status === "I") {
                con += "<td><button type=\"button\" class=\"btn btn-primary btn-xs\" onclick=\"pay_recharge.recharge_query(" + item.Id + ");\" title=\"查询\"><i class=\"glyphicon glyphicon-zoom-in\"></i>查询</button></td>";
            }

            if (item.Status === "S" && item.RecordClass === "a") {
                con += "<td><button type=\"button\" class=\"btn btn-warning btn-xs\" onclick=\"pay_recharge.recharge_notice(" + item.Id + ");\" title=\"回调\"><i class=\"glyphicon glyphicon-share\"></i>回调</button></td>";
            }

            con += "</tr>";
        });
        $("#your_showtime").html(con);
        $("#_s_recharge_amount").html(amount);
    },
    pay_query: function (no) {
        $.ajax({
            type: "GET",
            url: "/merchant/pay_query_v2/" + no,
            cache: false,
            success: function (res) {
                if (res.code === 9) {
                    swal("更新成功！", {
                        icon: "success",
                        closeOnClickOutside: false,
                    }).then(() => {
                        setInterval(function () {
                            window.location.reload();
                        }, 2000);
                    });
                } else {
                    swal(res.msg, {
                        icon: "warning",
                        closeOnClickOutside: false,
                    });
                }
            },
            error: function (XMLHttpRequest) {
                toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
            }
        });
    },
    pay_notice: function (no) {
        $.ajax({
            type: "GET",
            url: "/merchant/pay_notice/" + no,
            cache: false,
            success: function (res) {
                if (res.code === 9) {
                    swal(res.msg, {
                        icon: "success",
                        closeOnClickOutside: false,
                    }).then(() => {
                        setInterval(function () {
                            window.location.reload();
                        }, 2000);
                    });
                } else {
                    swal(res.msg, {
                        icon: "warning",
                        closeOnClickOutside: false,
                    });
                }
            },
            error: function (XMLHttpRequest) {
                toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
            }
        });
    },
    pay_do_paging: function () {
        let merchantNo = $("#merchant_No").val();
        let accountName = $("#accountName").val();
        let datetimepicker1 = $("#datetimepicker1").val();
        let datetimepicker2 = $("#datetimepicker2").val();
        let uStatus = $("#uStatus").val();
        let _userName = $("#_p_userName").val();
        $.ajax({ //去后台查询第一页数据
            type: "GET",
            url: "/merchant/list_pay/",
            data: {
                page: '1',
                limit: "20",
                MerchantNo: merchantNo,
                MerchantName: accountName,
                start: datetimepicker1,
                end: datetimepicker2,
                _userName: _userName,
                uStatus: uStatus,
            }, //参数：当前页为1
            success: function (data) {
                pay_recharge.show_pay_data(data.root, data._p_rechargeT, data.auth);//处理第一页的数据

                let options = {//根据后台返回的分页相关信息，设置插件参数
                    bootstrapMajorVersion: 3, //
                    currentPage: data.page, //当前页数
                    totalPages: data.totalPage, //总页数
                    numberOfPages: data.limit,//每页记录数
                    itemTexts: function (type, page) {//设置分页按钮显示字体样式
                        switch (type) {
                            case "first":
                                return "首页";
                            case "prev":
                                return "上一页";
                            case "next":
                                return "下一页";
                            case "last":
                                return "末页";
                            case "page":
                                return page;
                        }
                    },
                    onPageClicked: function (event, originalEvent, type, page) {//分页按钮点击事件
                        $.ajax({//根据page去后台加载数据
                            url: "/merchant/list_pay/",
                            type: "GET",
                            data: {
                                page: page,
                                MerchantNo: merchantNo,
                                MerchantName: accountName,
                                start: datetimepicker1,
                                end: datetimepicker2,
                                uStatus: uStatus,
                                _userName: _userName,
                            },
                            success: function (data) {
                                pay_recharge.show_pay_data(data.root, data._p_rechargeT, data.auth);//处理数据
                            }
                        });
                    }
                };

                $('#xf_page').bootstrapPaginator(options);//设置分页
            },
            error: function (XMLHttpRequest) {
                toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
            }
        });
    },
    show_pay_data: function (list, amount, auth) {
        let con = "";
        $.each(list, function (index, item) {
            let st = "";
            switch (item.Status) {
                case "F":
                    st = "失败";
                    break;
                case "I":
                    st = "处理中";
                    break;
                case "S":
                    st = "成功";
                    break
            }
            let u = "";
            switch (item.WhUserType) {
                case "2":
                    u = "对公";
                    break;
                case "1":
                    u = "对私";
                    break;
            }

            con += "<tr><td><a>" + (index + 1) + "</a></td>";
            con += "<td>" + item.UserName + "</td>";
            con += "<td>" + item.WhOrderId + "</td>";
            con += "<td>" + item.WhAmount + "</td>";
            con += "<td>" + item.WhAccountNo + "</td>";
            con += "<td>" + item.WhAccountName + "</td>";
            con += "<td>" + item.EditTime + "</td>";
            con += "<td>" + u + "</td>";
            con += "<td>" + st + "</td>";
            con += "<td>" + item.Remark + "</td>";

            if (item.Status === "I") {
                con += "<td><button type=\"button\" class=\"btn btn-primary btn-xs\" onclick=\"pay_recharge.pay_query(" + item.Id + ");\" title=\"查询\"><i class=\"glyphicon glyphicon-zoom-in\"></i>查询</button></td>";
            }

            if (item.Status === "S" && item.RecordClass === "a") {
                con += "<td><button type=\"button\" class=\"btn btn-warning btn-xs\" onclick=\"pay_recharge.pay_notice(" + item.Id + ");\" title=\"回调\"><i class=\"glyphicon glyphicon-share\"></i>回调</button></td>";
            }

            if (auth <= 100 && (item.Status === "" || item.Status === "I")) {
                con += "<td><button type=\"button\" class=\"btn btn-danger btn-xs\" onclick=\"pay_recharge.hand_pay_record_ui(" + item.Id + ");\" title=\"回调\"><i class=\"glyphicon glyphicon-share\"></i>手动处理为失败</button></td>";
            }

            con += "</tr>";
        });
        $("#your_showtime").html(con);
        $("#_p_recharge_amount").html(amount);
    },
    send_msg_do_pay: function () {
        let accountNo = $("#accountNo").val();
        let accountName = $("#accountName").val();
        let money = $("#money").val();

        let patrn2 = /^(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))$/;

        if (accountNo === "" || accountName === "") {
            toastr.error("银行卡或户名不能为空!");
        } else if (!patrn2.exec(money)) {
            toastr.error("请输入正确的提现金额哦!");
        } else {
            $.ajax({
                type: "POST",
                url: "/merchant/do_pay_sms/",
                data: $("#pay_form").serialize(),
                cache: false,
                success: function (res) {
                    if (res.code === 9) {
                        toastr.success(res.msg);
                    } else {
                        toastr.error(res.msg);
                    }
                },
                error: function (XMLHttpRequest) {
                    toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
                }
            });
        }
    },
    hand_pay_record_ui: function (Id) {
        $.ajax({
            type: "GET",
            url: "/user/hand_pay_record_ui/" + Id,
            cache: true,
            success: function (res) {
                if (res.code === 9) {
                    $('#charge_record_status_modal').modal({backdrop: 'static', keyboard: false});
                } else {
                    toastr.error("此订单不存在！");
                }
            },
            error: function (XMLHttpRequest) {
                toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
            }
        });
    },
    hand_pay_record: function () {
        let record_status = $("input[type='radio']:checked").val();

        swal({
            title: "进行手动查单",
            text: "先向上游确认此笔订单状态了吗，已确认过点击OK！没有则点击Cancel！",
            icon: "warning",
            closeOnClickOutside: false,
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: "POST",
                    url: "/user/hand_pay_record/",
                    data: {
                        recordStatus: record_status
                    },
                    success: function (res) {
                        if (res.code === 9) {
                            swal("此订单已成功处理！", {
                                icon: "success",
                            }).then(() => {
                                setTimeout(function () {
                                    window.location.reload();
                                }, 2000);
                            });
                        } else {
                            swal(res.msg, {
                                icon: "warning",
                            });
                        }
                    },
                    error: function (XMLHttpRequest) {
                        swal('something is wrong, code:' + XMLHttpRequest.status, {
                            icon: "warning",
                        });
                    }
                });
            }
        });
    },
};